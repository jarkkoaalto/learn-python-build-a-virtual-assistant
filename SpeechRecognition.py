#  Speachregnition.py
#  Application recognition your speach using speachregnition-library and answers using espeak library
# 1. When app start machine say "Welcome"
# 2. App recognize your voice 
# 3. Wolframalpa repeat "anwer is : "
# 4. Wikipedia repat "Searched for + what search"
#

import wx
import wikipedia
import wolframalpha
from espeak import espeak
import speech_recognition as sr

espeak.synth("Welcome ")

class MyFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None,
            pos=wx.DefaultPosition, size=wx.Size(500, 100),
            style=wx.MINIMIZE_BOX | wx.SYSTEM_MENU | wx.CAPTION |
             wx.CLOSE_BOX | wx.CLIP_CHILDREN,
            title="PyVir")
        panel = wx.Panel(self)
        my_sizer = wx.BoxSizer(wx.VERTICAL)
        lbl = wx.StaticText(panel,
        label="Hello I am PyVir the Python Virtual Digital Assistant. How can I help you?")
        my_sizer.Add(lbl, 0, wx.ALL, 5)
        self.txt = wx.TextCtrl(panel, style=wx.TE_PROCESS_ENTER,size=(500,30))
        self.txt.SetFocus()
        self.txt.Bind(wx.EVT_TEXT_ENTER, self.OnEnter)
        my_sizer.Add(self.txt, 0, wx.ALL, 5)
        panel.SetSizer(my_sizer)
        self.Show()

    def OnEnter(self, event):
        input = self.txt.GetValue()
        input = input.lower()
        if input == '':
            r = sr.Recognizer()
            with sr.Microphone() as source:
                audio = r.listen(source)
        try:
            self.txt.SetValue(r.recognize_google(audio))
        except sr.UnknownValueError:
            print("Google Speech Recognition could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from Google Speach Recognition service; {0}".format(e))
        try:
            #wolframalpha
            app_id = "44-your-own-id-55"
            client = wolframalpha.Client(app_id)
            res = client.query(input)
            answer = next(res.results).text
            print answer
            espeak.synth("The answer is " + answer)
        except:
            #wikipedia
            input = input.split(' ')
            input = " ".join(input[2:])
            espeak.synth("Searched for "+input)
            print wikipedia.summary(input)


if __name__ == "__main__":
    app = wx.App(True)
    frame = MyFrame()
app.MainLoop()